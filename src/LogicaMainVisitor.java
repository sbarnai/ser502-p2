
import java.util.HashMap;
import java.util.Map;
import org.stringtemplate.v4.*;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.io.LineNumberReader;
import java.io.FileReader;
import org.antlr.v4.runtime.tree.ParseTree;
import java.util.*;

public class LogicaMainVisitor extends LogicaBaseVisitor<T> {
    String filename = "intermediate.mvm";
    int count = 0;
    String AssignmentID = "";
    String functionID = "";
    List<String> all_ids = new ArrayList<String>();
    boolean fun = false;

  //#############################USERDEFINED FUNCTIONS##################################

  public void appendResult(String res){
        try{
        Writer output;
        PrintWriter pwFile = null;
        output = new BufferedWriter(new FileWriter(filename, true));
        output.append(res);
        output.close();
        }catch(FileNotFoundException e) {
            e.printStackTrace();
        }catch(IOException e) {
            e.printStackTrace();
        }
  }

    public void checkIfEmpty(){
      if(count == 0){
        try{
            PrintWriter writer = new PrintWriter(filename);
           writer.print("");
           writer.close();
           count++;

          }catch(FileNotFoundException e) {
                e.printStackTrace();
          }catch(SecurityException e) {
                e.printStackTrace();
          }
      }
    }
    

  //###############################FUNCTIONS OVERRIDEN FROM PARENT########################

    @Override
    public T visitProgram(LogicaParser.ProgramContext ctx) {
        super.visitProgram(ctx);
        return T.VOID;
    }

    @Override
    public T visitMain(LogicaParser.MainContext ctx) {
        super.visitMain(ctx);
        return T.VOID ;
    }
    

    @Override
    public T visitBegin(LogicaParser.BeginContext ctx) {
        super.visitBegin(ctx);
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("beginst");
        String result = st.render();
        appendResult(result);
        return T.VOID ;
    }

    @Override
    public T visitEnd(LogicaParser.EndContext ctx) {
        super.visitEnd(ctx);
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("endst");
        String result = st.render();
        appendResult(result);
        return T.VOID ;
    }

    @Override
    public T visitBody(LogicaParser.BodyContext ctx) {
        T body = super.visitBody(ctx);
        return body;
    }

    @Override
    public T visitDeclaration(LogicaParser.DeclarationContext ctx) {
        super.visitDeclaration(ctx);
        return T.VOID ;
    }

    @Override
    public T visitNumDecl(LogicaParser.NumDeclContext ctx) {
        super.visitNumDecl(ctx);
        
        String id = ctx.IDENTIFIER().getText();
        
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("numDeclst");

        st.add("id", "\"" + id + "\"");
        
        String result = st.render();
        appendResult(result);
        
        return T.VOID ;
    }
    
    
    @Override
    public T visitLetter(LogicaParser.LetterContext ctx) {
        return new T(String.valueOf(ctx.getText()));
    }

    @Override
    public T visitCharDecl(LogicaParser.CharDeclContext ctx) {
        super.visitCharDecl(ctx);
        
        String id = ctx.IDENTIFIER().getText();
        String value = ctx.letter().getText();

        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("charDeclst");
        
        st.add("id", "\"" + id + "\"");
        st.add("value", value );
        
        String result = st.render();
        appendResult(result);
     
        return T.VOID ;
    }

    
    @Override
    public T visitBooleanVal(LogicaParser.BooleanValContext ctx) {
        return new T(Boolean.valueOf(ctx.getText()));
    }
    
    @Override
    public T visitBoolDecl(LogicaParser.BoolDeclContext ctx) {
        super.visitBoolDecl(ctx);
        
        String id = ctx.IDENTIFIER().getText();
        String value = ctx.booleanVal().getText();
        
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("boolDeclst");
        
        st.add("id", "\"" + id + "\"");
        st.add("value", value );
        
        String result = st.render();
        appendResult(result);
        
        return T.VOID ;
    }

    @Override
    public T  visitStringVal(LogicaParser.StringValContext ctx) {
        return new T(String.valueOf(ctx.getText()));
    }

    
    @Override
    public T visitStringDecl(LogicaParser.StringDeclContext ctx) {
        super.visitStringDecl(ctx);
        
        String id = ctx.IDENTIFIER().getText();
        String value = ctx.stringVal().getText();
        
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("stringDeclst");
        
        st.add("id", "\"" + id + "\"");
        st.add("value", value );
        
        String result = st.render();
        appendResult(result);
        
        return T.VOID ;
    }



    @Override
    public T visitStatement(LogicaParser.StatementContext ctx) {
        super.visitStatement(ctx);
        return T.VOID ;
    }


    @Override
    public T visitParanthesis(LogicaParser.ParanthesisContext ctx) {
        super.visitParanthesis(ctx);
        return T.VOID ;
    }
    
    @Override public T visitExprVal(LogicaParser.ExprValContext ctx) {
        super.visitExprVal(ctx);
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("operationst");
        if (!ctx.getText().contains (">>")){
            st.add ("val", ctx.getText());
            String result = st.render();
            
            appendResult(result );
        }



        return new T(String.valueOf(ctx.getText()));
    }
    
    
    @Override
    public T visitMultiply(LogicaParser.MultiplyContext ctx) {
        super.visitMultiply(ctx);
        
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("operatorst");
        st.add ("op", "*");
        String result = st.render();
        appendResult(result );
        return T.VOID;
    }

    @Override
    public T visitDivide(LogicaParser.DivideContext ctx) {
        super.visitDivide(ctx);
        
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("operatorst");
        st.add ("op", "/");
        String result = st.render();
        appendResult(result );
        
        return T.VOID ;
    }
    
    @Override
    public T visitAdd(LogicaParser.AddContext ctx) {
        super.visitAdd(ctx);
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("operatorst");
        st.add ("op", "+");
        String result = st.render();
        appendResult(result );
        return T.VOID ;
  }
    
    @Override
    public T visitSubtract(LogicaParser.SubtractContext ctx) {
        super.visitSubtract(ctx);
        
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("operatorst");
        st.add ("op", "-");
        String result = st.render();
        appendResult(result );
        
        return T.VOID ;
    }
    
    @Override public T visitFunction(LogicaParser.FunctionContext ctx) {
        super.visitFunction(ctx);
        String id = ctx.IDENTIFIER().getText();

        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("funst");
        st.add ("id", "\"" + id + "\"");
        
        String result = st.render();
        
        appendResult(result);
        
        
        return T.VOID;
    }

    
    @Override public T visitBeginfun(LogicaParser.BeginfunContext ctx) {
        
        super.visitBeginfun(ctx);
        fun = true;
        T param = this.visit(ctx.param());
        String[] one_param = param.toString().split(":");
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("beginfunst");
        st.add("param", "\"" + one_param[1] + "\"");
        String result = st.render();
        appendResult(result);
        
        return new T(String.valueOf(ctx.getText()));
    }
    
    @Override public T visitTyperr(LogicaParser.TyperrContext ctx) {
        super.visitTyperr(ctx);
        return new T(String.valueOf(ctx.getText()));
    }
    
    @Override public T visitParam(LogicaParser.ParamContext ctx) {
        super.visitParam(ctx);
        return new T(String.valueOf(ctx.getText()));
    }
    
    @Override public T visitReturnst(LogicaParser.ReturnstContext ctx) {
        super.visitReturnst(ctx);
        return T.VOID;
    }
    
    //// FUNCTION CALL /////////
    
    
    @Override public T visitFunctioncall(LogicaParser.FunctioncallContext ctx) {
        functionID = ctx.IDENTIFIER().getText();
        super.visitFunctioncall(ctx);
        
        
        return T.VOID;
    }
    
    @Override public T visitFunVals(LogicaParser.FunValsContext ctx) {
        super.visitFunVals(ctx);
        
        if (ctx.getText().matches("^-?\\d+$")){
            STGroup group = new STGroupFile("stringTemplate.stg");
            ST st = group.getInstanceOf("funcallExpr");
            st.add("id", functionID);
            st.add("val", ctx.getText());
        
            String result = st.render();
            appendResult(result);
        }
        else {
            STGroup group = new STGroupFile("stringTemplate.stg");
            ST st = group.getInstanceOf("funcallExpr");
            st.add("id", functionID);
            
            String result = st.render();
            appendResult(result);
            
        }
        
        return new T(String.valueOf(ctx.getText()));
    }
    
    
    @Override public T visitVals(LogicaParser.ValsContext ctx) {
        super.visitVals(ctx);
        return new T(String.valueOf(ctx.getText()));
    }
    
    //////////// IF /////////////////
    
    
    @Override
    public T visitThen(LogicaParser.ThenContext ctx) {
        super.visitThen(ctx);
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("thenst");
        String result = st.render();
        appendResult(result);
        return new T(String.valueOf(ctx.getText()));
    }
     
    //ifstatement 	: 	ifline then body end (elsest)?;
    @Override
    public T visitIfstatement(LogicaParser.IfstatementContext ctx) {
        super.visitIfstatement(ctx);
        return T.VOID;
    }
    
    //ifline 			: 	'if' '(' condition ')';
    @Override
    public T visitIfline(LogicaParser.IflineContext ctx) {
        super.visitIfline(ctx);
        return new T(String.valueOf(ctx.getText()));
    }
    
    
    @Override public T visitElsest(LogicaParser.ElsestContext ctx) {
        super.visitElsest(ctx);
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("elsest");
        String result = st.render();
        appendResult(result);
        return new T(String.valueOf(ctx.getText()));
    }
    
    //condition 		: 	identnumber opr identnumber;
    @Override
    public T visitCondition(LogicaParser.ConditionContext ctx) {
        T left = this.visit(ctx.identnumber(0));
        T right = this.visit(ctx.identnumber(1));
        T oper = this.visit(ctx.opr());
        
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("condst");
        
        st.add("op", oper);
        st.add("left", left.toString() );
        st.add("right", right.toString() );
        String result = st.render();
        
        appendResult(result);
        return T.VOID;
    }
    
    @Override public T visitOpr(LogicaParser.OprContext ctx) {
        super.visitOpr(ctx);
        return new T(String.valueOf(ctx.getText()));
    }
    
    @Override public T visitIdentnumber(LogicaParser.IdentnumberContext ctx) {
        super.visitIdentnumber(ctx);
        return new T(String.valueOf(ctx.getText()));
        
    }
    
    ///// Assignment /////
    
    @Override public T visitAssignst(LogicaParser.AssignstContext ctx) {
        AssignmentID = ctx.IDENTIFIER().getText();
        super.visitAssignst(ctx);
        return T.VOID;
    }
    
    @Override public T visitAssignVal(LogicaParser.AssignValContext ctx) {
        super.visitAssignVal(ctx);
        
        
        if ( ctx.getText().matches("^-?\\d+$") ){
            STGroup group = new STGroupFile("stringTemplate.stg");
            ST st = group.getInstanceOf("assignForIntsOnly");
            st.add("val", ctx.getText() );

            String result = st.render();
            appendResult(result);
            
            
        }else if ( ctx.getText().contains ("+") || ctx.getText().contains ("-")
                  || ctx.getText().contains ("*") || ctx.getText().contains ("/")){

            all_ids.add( AssignmentID );
            boolean duplicate = false;
            
            for (int j = 0; j < all_ids.size(); j++) {
                for (int k = j+1; k < all_ids.size(); k++) {
                    if ( all_ids.get(k).equals(all_ids.get(j)) ){
                        duplicate = true;
                    }
                }
            }
            
            if (duplicate == false || fun == false){
                STGroup group = new STGroupFile("stringTemplate.stg");
                ST st = group.getInstanceOf("assignIDst");
                st.add("id", "\"" + AssignmentID + "\"");
                String result = st.render();
                appendResult(result);
            }

        } else if (ctx.getText().contains (">>") ){
            STGroup group = new STGroupFile("stringTemplate.stg");
            ST st = group.getInstanceOf("functionAssignmentST");
            st.add("id", "\"" + AssignmentID + "\"");
            String result = st.render();
        }
        
        
        return new T(String.valueOf(ctx.getText()));
    }
    
    
    ////////// WHILE (REPEAT) //////////////
    @Override public T visitRepeatLoop(LogicaParser.RepeatLoopContext ctx) {
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("repeatHeader");
        st.add ("n", "\"n\"" );
        String result = st.render();
        appendResult(result);
        
        super.visitRepeatLoop(ctx);

        ST st2 = group.getInstanceOf("repeatST");
        st2.add ("while", "\"while\"");
        String result2 = st2.render();
        appendResult(result2);
        
        T repeatNum = this.visit(ctx.times());
        ST st3 = group.getInstanceOf("repeatCall");
        st3.add ("while", "while");
        st3.add ("times", repeatNum);

        String result3 = st3.render();
        appendResult(result3);
        
        return T.VOID;
    }
    
    @Override public T visitTimes(LogicaParser.TimesContext ctx) {
        return new T (Integer.valueOf(ctx.getText()));
    }
    
    
    @Override public T visitEndRepeat(LogicaParser.EndRepeatContext ctx) {
        STGroup group = new STGroupFile("stringTemplate.stg");
        ST st = group.getInstanceOf("endRepeat");
        //st.add ("n", "\"n\"" );
        String result = st.render();
        appendResult(result);
        return T.VOID;
    }
    
    
    /////////////// STACK
    
    @Override
    public T visitStackDecl(LogicaParser.StackDeclContext ctx) {
        super.visitStackDecl(ctx);
        String val = this.visit(ctx.identnumber()).toString();
        appendResult("\""+val+"\"" + " stack ");
        return new T(String.valueOf(ctx.getText()));
    }
    
    //stackbody: (stackpush | stackpop | stackshow);
    @Override
    public T visitStackbody(LogicaParser.StackbodyContext ctx) {
        super.visitStackbody(ctx);
        return new T(String.valueOf(ctx.getText()));
    }
    
    //stackpush: 'push' identnumber input;
    @Override
    public T visitStackpush(LogicaParser.StackpushContext ctx) {
        super.visitStackpush(ctx);
        String val = this.visit(ctx.input()).toString();
        appendResult( val+ " pushval ");
        return new T(String.valueOf(ctx.getText()));
    }
    
    @Override public T visitInput(LogicaParser.InputContext ctx) {
        super.visitInput(ctx);
        return new T(String.valueOf(ctx.getText()));
    }
    @Override public T visitStackpop(LogicaParser.StackpopContext ctx) {
        super.visitStackpop(ctx);
        String val = this.visit(ctx.input()).toString();
        appendResult(" popval ");
        return new T(String.valueOf(ctx.getText()));
    }
    @Override public T visitStackshow(LogicaParser.StackshowContext ctx) {
        super.visitStackshow(ctx);
        appendResult(" printval ");
        return new T(String.valueOf(ctx.getText()));
    }
    
    ///////////////// WRITE
    
    
    @Override public T visitWrite(LogicaParser.WriteContext ctx) {
        super.visitWrite(ctx);
        appendResult(" show \n");
        return T.VOID;
    }
    
    @Override public T visitWriteBody(LogicaParser.WriteBodyContext ctx){
        appendResult(ctx.getText());
        return new T(String.valueOf(ctx.getText()));
    }

}