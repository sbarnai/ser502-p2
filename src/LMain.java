
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.FileInputStream;
import java.io.InputStream;

public class LMain {
    public static void main(String[] args) throws Exception {
        String inputFile = null;
        if ( args.length>0 ) inputFile = args[0];
        InputStream is = System.in;
        if ( inputFile!=null ) is = new FileInputStream(inputFile);
        ANTLRInputStream input = new ANTLRInputStream(is);
        LogicaLexer lexer = new LogicaLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        LogicaParser parser = new LogicaParser(tokens);
        ParseTree tree = parser.program(); // parse

        LogicaMainVisitor visitor = new LogicaMainVisitor();
        visitor.checkIfEmpty();
        visitor.visit(tree);
    }
}
