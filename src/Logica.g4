grammar Logica;

program			:	main (function+)? (body+)? end;

main			:	'main' '(' ')' begin (body+)? ;	        	

body			: 	declaration                                   
				|	body declaration 
				|	statement
				|	body statement
				| 	expr 
				|	body expr
				|	NUMBER
				|	stackbody
				|	body stackbody
				;

declaration 	:   numDecl 
				| 	charDecl 
				| 	boolDecl  
				| 	stringDecl 
				;

numDecl			:	'int:'  (IDENTIFIER | LETTER) '=' assignVal  ;

charDecl	 	:	'char:' (IDENTIFIER | LETTER) '=' letter ;

boolDecl	 	:	'bool:' (IDENTIFIER | LETTER) '=' booleanVal ;

booleanVal		:	'True'	
				|	'False'
				;

stringDecl		:	'string:' (IDENTIFIER | LETTER) '=' stringVal ;

typerr			: 	('int:' | 'char:' | 'bool:' | 'string:' | 'void:');

vals			: 	(LETTER | NUMBER | IDENTIFIER | 'True' | 'False' | STRING | expr);

statement 		: 	ifstatement
            	| 	repeatLoop
            	| 	assignst
            	| 	functioncall
            	|	NUMBER
            	|	write
            	;

assignst 		: 	(IDENTIFIER | LETTER) '=' assignVal ;

assignVal		: 	vals
				|	expr
				|	functioncall
				;

ifstatement 	: 	ifline then body end (elsest)?;

ifline 			: 	'if' '(' condition ')';

elsest			: 	'else' then body end;

condition 		: 	identnumber opr identnumber;

opr				: 	('=='|'<'|'>');

identnumber 	: 	(IDENTIFIER | LETTER | NUMBER);

repeatLoop		: 	'repeat' times ':' body endRepeat;

times			:	NUMBER;

function		: 	typerr (IDENTIFIER | LETTER) beginfun body (returnst)? end;

returnst		: 	'return' (expr | IDENTIFIER | LETTER);

param 			: 	typerr (IDENTIFIER | LETTER);

functioncall	: 	'>>' (IDENTIFIER | LETTER) '(' (funVals)? ')';

funVals 		:	IDENTIFIER
				|	NUMBER
				|	expr
				;

expr			:   exprVal '*' exprVal 		#Multiply
				|	exprVal '/' exprVal 		#divide
   				|   exprVal '+' exprVal  		#Add
   				|	exprVal '-' exprVal			#Subtract
   				|   '(' exprVal ')'        		#Paranthesis
    			;

exprVal			:	NUMBER 				
   				|	IDENTIFIER	
   				|	functioncall
   				;

write 			:	'write:' writeBody;

writeBody		:	STRING
				|	expr
				|	IDENTIFIER
				;

stackbody		: (stackpush | stackpop | stackshow | stackDecl);

stackDecl	  	: 'stack:' identnumber;

stackpush		: 'push:' identnumber input;

input			: identnumber;

stackpop		: 'pop:' input ;

stackshow		: 'print:' IDENTIFIER;

begin			: 	'begin:' ;

end				:	'end'   ;

endmain			:	'end'	;

then			:	'then:'	;

loop			:	'loop:'	;

beginfun		:	'(' ( param)? ')' 'begin:';

beginLoop		: 	'begin:';

endRepeat		:	'end';

letter 			:	LETTER	;
stringVal 		:	STRING ;
TRUE 			:	'True';
FALSE 			:	'False';
IDENTIFIER 		:   [a-z]+ | [A-Z]+  ;
LETTER 			:	'\'' ([a-z] | [A-Z])  '\'' ;
STRING 			:	'"' ( WS | ~('\\'|'"') )* '"' ;
NUMBER 			:   [0-9]+   ;
WS  			:  	(' ' | '\t' | '\n' )+ {skip();};
