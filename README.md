#SER 502 PROJECT(2) TEAM(5)#
#LOGICA Programming Language#
===========================================

####To watch the video, please click [here](https://www.youtube.com/watch?v=0hypIAXsco4)..####
####To download the power point presentation, please click [here](https://www.ipage.com/controlpanel/FileManager/m.cmp?cmd=dlfile&p=Logica_Presentation.pptx)..####
####To download the manual, please click [here](https://www.ipage.com/controlpanel/FileManager/m.cmp?cmd=dlfile&p=Logica_Manual.pdf)..####
####To download the language (jar file), please click [here](https://www.ipage.com/controlpanel/FileManager/m.cmp?cmd=dlfile&p=Logica-Team5.jar)..####

##HOW TO INSTALL AND RUN LOGICA:##
1. Download the package    
2. From your terminal:    
	`cd Logica-Team5`    
	`./Logica_installer.sh`     
3- Running Logica_installer.sh installs all dependencies needed to run Logica.    
After that, you can use the keyword “Logica” to run any file with the    
extension “.lgc”. For example:      
  	`$ Logica helloworld.lgc`    
===============================================

###Done By:
- Zahra Al-Kaf              1207315300  
- Subhransu Mishra 	1207679482  
- Sylvia Barnai             1203630020   
- Akkshaya Gopalakrishnan	1207926079    

=================================================