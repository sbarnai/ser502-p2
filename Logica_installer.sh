#!/bin/sh
mkdir lang
rsync -av src /usr/local/lib/lang
rsync -av Logica /usr/local/lib/lang
rsync -av /usr/local/lib/lang/Logica  /usr/local/bin/Logica

cd /usr/local/lib
echo "INSTALLING DEPENDENCIES...."
sudo curl -O http://www.antlr.org/download/antlr-4.5-complete.jar
export CLASSPATH=".:/usr/local/lib/antlr-4.5-complete.jar:$CLASSPATH"
#alias antlr4='java -jar /usr/local/lib/antlr-4.5-complete.jar'
echo $antlr4
rsync -av /usr/local/lib/antlr-4.5-complete.jar  /usr/local/bin/antlr4

sudo curl -O http://www.stringtemplate.org/download/ST-4.0.8.jar
export CLASSPATH=".:/usr/local/lib/ST-4.0.8.jar:wq:$CLASSPATH"
cd
